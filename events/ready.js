module.exports = {
    name: 'ready',
    description: 'The reply you get back from the API when the bot is ready to serve',
    execute() {
        console.log("Ready to receive commands");

    }
}